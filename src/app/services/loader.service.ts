import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

const MOCK_DURATION: number = 2000;

@Injectable({
    providedIn: 'root',
})
export class LoaderService {
    private isLoading: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor() { }

    public getLoaderStatus(): Observable<boolean> {
        return this.isLoading.asObservable();
    }

    public resetLoaderParams(): void {
        this.isLoading.next(false);
    }

    public getStartLoading(): void {
        this.isLoading.next(true);
    }

    public endLoading(): void {
        setTimeout(() => {
            this.isLoading.next(false);
        }, MOCK_DURATION);
    }
}
