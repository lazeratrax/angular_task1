import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Course, CoursesPageParams } from '../interfaces/course.interface';

const PAGE_SIZE: number = 3;
const DEFAULT_PARAMS: CoursesPageParams =
{
  start: 0,
  count: PAGE_SIZE
};

@Injectable({
  providedIn: 'root',
})
export class CourseService {

  private storedCourses: Course[] = [];

  private courseSubjectSource: BehaviorSubject<CoursesPageParams> =
    new BehaviorSubject<CoursesPageParams>(DEFAULT_PARAMS);

  constructor(private http: HttpClient) { }

  public getPageParams(): Observable<CoursesPageParams> {
    return this.courseSubjectSource.asObservable();
  }

  public setPageParams(params: CoursesPageParams, isReset?: boolean): void {
    if (Boolean(isReset) === true) {
      this.storedCourses = [];
    }
    this.courseSubjectSource.next(params);
  }

  public loadCourse(): void {
    const val: CoursesPageParams = this.courseSubjectSource.getValue();
    if (Boolean(val.count) === true) {
      this.setPageParams({ ...val, start: val.start + val.count });
    }
  }

  public updateStoredCourses(courses: Course[]): Course[] {
    return this.storedCourses = this.storedCourses.concat(courses);
  }

  public resetPageParams(): void {
    this.storedCourses = [];
    this.courseSubjectSource.next(DEFAULT_PARAMS);
  }

  public getCoursesPage(coursesPageParams: CoursesPageParams): Observable<Course[]> {
    let params: HttpParams;
    (Boolean(coursesPageParams.count) === true) ? (
      params = new HttpParams()
        .set('count', coursesPageParams.count?.toString())
        .set('start', coursesPageParams.start?.toString())) :
      (params = new HttpParams()
        .set('textFragment', coursesPageParams.textFragment?.toString()));
    return this.http.get<Course[]>(`/courses`, { params, responseType: 'json' });
  }

  public getList(): Observable<Course[]> | null {
    return this.http.get<Course[]>(`/courses`);
  }

  public removeItem(id: number): Observable<void> {
    return this.http.delete<void>(`/courses/${id}`);
  }

  public createCourse(course: Course): Observable<Course> {
    return this.http.post<Course>(`/courses`, course);
  }

  public getItemById(id: number): Observable<Course> {
    return this.http.get<Course>(`/courses/${id}`);
  }
}

