import { TestBed } from '@angular/core/testing';
import { CourseService } from './course.service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Course } from '../interfaces/course.interface';

export interface ImockCourses {
  id: number;
  topRated: boolean;
  title: string;
  creationDate: string;
  description: string;
}

const mockDate: string = '2020-09-28T04:39:24+00:00';
const mockCourses: ImockCourses = {
  id: 3,
  topRated: false,
  title: 'test',
  creationDate: mockDate,
  description:
    'text d'
};

describe('CourseService', () => {
  // tslint:disable-next-line:prefer-const
  let http: HttpClient;
  let testCourseService: CourseService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        CourseService
      ],
    });

    testCourseService = new CourseService(http);
  });

  it('can work getList()', () => {
    spyOn(testCourseService, 'getList').and.callThrough();
    const service: () => Observable<Course[]> = testCourseService.getList;
    expect(service).toHaveBeenCalled();
  });
});
