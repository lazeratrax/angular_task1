import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Token, User } from '../interfaces/auth.interfaces';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<User> {
    return this.http
      .post<User>(`/auth/login`, user);
  }

  public getUserInfo(token: Token): Observable<User> | null {
    return this.http.post<User>(`/auth/userinfo`, token);
  }

  public logout(): void {
    localStorage.clear();
  }

  public isAuthenticated(): boolean {
    return !Boolean(localStorage.getItem('token'));
  }
}
