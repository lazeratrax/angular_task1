export const mockCourses = [
  {
    id: 1,
    topRated: true,
    title: 'Video Course 1',
    creationDate: new Date(2020, 10, 28, 17, 23, 42),
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    id: 2,
    topRated: false,
    title: 'Video Course 2',
    creationDate: new Date(2020, 8, 7, 17, 23, 4),
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
  {
    id: 3,
    topRated: false,
    title: 'Video Course 3. tag',
    creationDate: new Date(2020, 9, 26, 0, 23, 5),
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
  },
];
