
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';

const routes: Routes = [
  {
    path: 'courses',
    loadChildren: () => import('./courses-module/course.module').then(
      (m: typeof import('./courses-module/course.module')) => m.CourseModule)
  },
  { path: '', redirectTo: 'courses', pathMatch: 'full' },
  {
    path: 'auth/login',
    component: LoginPageComponent
  },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
