import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-text-input-for-now',
  templateUrl: './text-input-for-now.component.html',
  styleUrls: ['./text-input-for-now.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TextInputForNowComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
