import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { Course, Author } from 'src/app/interfaces/course.interface';
import { CourseService } from 'src/app/services/course.service';

export const NUM: number = 10e4;

@Component({
  selector: 'app-add-page',
  templateUrl: './add-page.component.html',
  styleUrls: ['./add-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddPageComponent implements OnInit {

  public form: FormGroup;
  public lengthCourse: number;

  constructor(
    private courseService: CourseService
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      name: new FormControl(null, Validators.required),
      description: new FormControl(null, Validators.required),
      date: new FormControl(null, Validators.required),
      length: new FormControl(null, Validators.required),
      authors: new FormControl(null, Validators.required)
    });
  }

  public submitForm(): void {
    const newIid: number = new Date().getTime() % NUM;
    const mockAuthor: Author =
    {
      id: 8413,
      name: 'Greta',
      lastName: 'Richardson'
    };

    const course: Course = {
      id: newIid,
      name: this.form.value.name,
      description: this.form.value.description,
      isTopRated: false,
      date: this.form.value.date,
      authors: [mockAuthor],
      length: this.form.value.length,
    };

    this.courseService.createCourse(course)
      .subscribe(
        (params: Course) => {
          console.log('params', params);
        });
  }
}
