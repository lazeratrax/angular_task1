import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-input-in-minutes',
  templateUrl: './input-in-minutes.component.html',
  styleUrls: ['./input-in-minutes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputInMinutesComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
