import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from '@angular/core';

@Component({
  selector: 'app-modal-delete',
  templateUrl: './modal-delete.component.html',
  styleUrls: ['./modal-delete.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModalDeleteComponent implements OnInit {
  @Output() cancel: EventEmitter<void> = new EventEmitter<void>();
  @Output() confirm: EventEmitter<void> = new EventEmitter<void>();
  public ngOnInit(): void { }

  public clickCancel(): void {
    this.cancel.emit();
  }

  public clickConfirm(): void {
    this.confirm.emit();
    this.clickCancel();
  }
}
