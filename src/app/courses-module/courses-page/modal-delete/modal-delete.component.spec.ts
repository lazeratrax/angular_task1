
import { Component } from '@angular/core';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { ModalDeleteComponent } from './modal-delete.component';

@Component({
    template: `
    <app-modal-delete></app-modal-delete>
      `
})
class TestModalDeleteComponent {
}

describe('ListCourses', () => {
    let fixture: ComponentFixture<TestModalDeleteComponent>;
    let testHost: TestModalDeleteComponent;
    let confirmBtn: Element;
    beforeEach(async(() => {
        TestBed
            .configureTestingModule({ declarations: [ModalDeleteComponent, TestModalDeleteComponent] });
        fixture = TestBed.createComponent(TestModalDeleteComponent);
        testHost = fixture.componentInstance;
        confirmBtn = fixture.nativeElement.querySelector('.confirm');
        fixture.detectChanges();
    }));

    it('should display text content', () => {
      const testString: string = 'Yes';
      expect(confirmBtn.textContent).toContain(testString);
    });
});
