import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CoursesPageComponent } from './courses-page.component';

describe('CoursesPageComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        CoursesPageComponent
      ],
    }).compileComponents();
  }));

  it('should create the courses page', () => {
    const fixture: ComponentFixture<CoursesPageComponent> = TestBed.createComponent(CoursesPageComponent);
    const CoursesPage: CoursesPageComponent = fixture.componentInstance;
    expect(CoursesPage).toBeTruthy();
  });
});
