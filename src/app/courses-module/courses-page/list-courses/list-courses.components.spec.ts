import { TestBed, async } from '@angular/core/testing';
import { ListCoursesComponent } from './list-courses.component';
import { CourseService } from './../../../services/course.service';
import { Course, CoursesPageParams } from 'src/app/interfaces/course.interface';
import * as VARIABLES from './../../variables';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

const MOCK_INIT_NUMBER: number = 3;
describe('ListCourses', () => {
  let component: ListCoursesComponent;
  let service: CourseService;

  // tslint:disable-next-line:prefer-const
  let http: HttpClient;
  const mockCourses: Course[] = [{
    id: VARIABLES.MOCK_ID,
    isTopRated: false,
    name: `${VARIABLES.MOCK_TITLE}`,
    date: `${VARIABLES.MOCK_CREATION_DATE}`,
    description: `${VARIABLES.MOCK_DESCRIPTION}`,
    length: VARIABLES.MOCK_LENGTH
  }];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ListCoursesComponent
      ],
    });
    service = new CourseService(http);
    component = new ListCoursesComponent(service);
  }));

  it('should create the list of courses', () => {
    expect(component).toBeTruthy();
  });

  it('should call getList() when ngOnInit', () => {
    const spy: jasmine.Spy<() => Observable<Course[]>> = spyOn(service, 'getList');
    spy.and.returnValue(new Observable<typeof mockCourses>());
    expect(spy()).toEqual(new Observable<typeof mockCourses>());
  });

  it('should call getItemById() when called courseId()', () => {
    const spy: jasmine.Spy<(id: number) => Observable<Course>> = spyOn(service, 'getItemById');
    spy.and.returnValue(new Observable[VARIABLES.MOCK_ID]<typeof mockCourses>());
    expect(spy(MOCK_INIT_NUMBER)).toEqual(new Observable[VARIABLES.MOCK_ID]<typeof mockCourses>());
  });

  it('should call service.removeItem() when called itemDelete()', () => {
    const spy: jasmine.Spy<(id: number) => Observable<void>> = spyOn(service, 'removeItem');
    component.itemDelete();
    spy.and.returnValue(null);
    expect(spy(MOCK_INIT_NUMBER)).toEqual(null);
  });
});
