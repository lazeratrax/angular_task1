import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, filter, map, switchMap } from 'rxjs/operators';
import { CourseService } from '../../../services/course.service';
import { Course, CoursesPageParams } from 'src/app/interfaces/course.interface';

const DEBOUNCE_TIME: number = 1000;
@Component({
  selector: 'app-list-courses',
  templateUrl: './list-courses.component.html',
  styleUrls: ['./list-courses.component.scss']
})
export class ListCoursesComponent implements OnInit, OnDestroy {

  public courses$: Observable<Course[]>;
  public modal: boolean = false;
  public courseIdToDelete: number;

  constructor(private courseService: CourseService) { }

  public ngOnInit(): void {
    this.courses$ = this.courseService.getPageParams().pipe(
      filter((res: CoursesPageParams) =>
        Object.values(res).length === 2 || Object.values(res)[0].length > 2
      ),
      switchMap((params: CoursesPageParams) => this.courseService.getCoursesPage(params)),
      map((data: Course[]) => this.courseService.updateStoredCourses(data)),
      debounceTime(DEBOUNCE_TIME),
      distinctUntilChanged()
    );
  }

  public loadMoreCourse(): void {
    this.courseService.loadCourse();
  }

  public onSearchChange(textFragment: string): void {
    this.courseService.setPageParams({ textFragment }, true);
  }

  public clickToggleModal(courseId: number): void {
    this.courseIdToDelete = courseId;
  }

  public itemDelete(): void {
    this.courseService.removeItem(this.courseIdToDelete)
      .subscribe(
        (params: void) => {
          this.courseService.resetPageParams();
        });
  }

  public closeDelModal(): void {
    this.courseIdToDelete = null;
  }

  public ngOnDestroy(): void {
    this.courseService.resetPageParams();
  }
}
