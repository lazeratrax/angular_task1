import { async, TestBed } from '@angular/core/testing';
import { Course } from 'src/app/interfaces/course.interface';
import { CourseItemComponent } from './course-item.component';
import * as VARIABLES from './../../../variables';

describe('CourseItemComponent', () => {
    let component: CourseItemComponent;
    let mockCourseService: Course[];
    const mockCourse: Course = {
        id: VARIABLES.MOCK_ID,
        isTopRated: false,
        name: `${VARIABLES.MOCK_TITLE}`,
        date: `${VARIABLES.MOCK_CREATION_DATE}`,
        description: `${VARIABLES.MOCK_DESCRIPTION}`,
        length: VARIABLES.MOCK_LENGTH
    };

    beforeEach(async(() => {
        mockCourseService = jasmine.createSpyObj(['getItemById']);
        mockCourseService.getItemById.and.returnValue(mockCourse);
        TestBed.configureTestingModule({
            providers: [{ provide: mockCourseService }]
        });
        component = new CourseItemComponent();
    }));

    it('toggleModal() should raise course values', () => {
        component.ngOnInit(
            component.course = mockCourseService.getItemById(VARIABLES.MOCK_ID)
        );

        component.clickToggleModal.subscribe(() => {
            expect(component.course).toEqual(mockCourse, 'course');
        });

        component.toggleModal();
    });
});
