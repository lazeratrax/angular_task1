import { DurationPipe } from './duration.pipe';
const MOCK_DATE: number = 2123114145;

describe('DurationPipe', () => {
  const pipe: DurationPipe = new DurationPipe();
  const testDate: number = MOCK_DATE;

  it('should have string "min"', () => {
    expect(pipe.transform(testDate)).toContain(`min`);
  });
});
