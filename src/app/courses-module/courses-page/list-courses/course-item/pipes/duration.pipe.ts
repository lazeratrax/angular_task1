import { Pipe, PipeTransform } from '@angular/core';

const CONVERT_TIME: number = 60;

@Pipe({
  name: 'duration',
})
export class DurationPipe implements PipeTransform {

  transform(duration: number): string {
    const hours: number = Math.floor(duration / CONVERT_TIME);
    const mins: number = Math.floor(duration % CONVERT_TIME);
    if (Math.floor(duration / CONVERT_TIME) >= 1) {
       return `${hours} h ${mins} min`;
    } else if (mins > 0) {
      return `${mins} min`;
    }
    else {
      return ` `;
    }
  }
}
