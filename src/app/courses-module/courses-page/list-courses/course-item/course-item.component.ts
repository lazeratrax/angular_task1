import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { IconDefinition } from '@fortawesome/fontawesome-svg-core';
import {
  faStar,
  faClock,
  faCalendarAlt,
} from '@fortawesome/free-solid-svg-icons';
import { Course } from '../../../../interfaces/course.interface';

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html',
  styleUrls: ['./course-item.component.scss'],
})
export class CourseItemComponent {
  public faStar: IconDefinition = faStar;
  public faClock: IconDefinition = faClock;
  public faCalendarAlt: IconDefinition = faCalendarAlt;

  public currentDate: number = Date.now();

  @Input() course: Course;
  @Output() clickToggleModal: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  public toggleModal(): void {
    this.clickToggleModal.emit(this.course.id);
  }

  public courseEdit(): void {
  }

  public itemDelete(id: number): void {
  }
}
