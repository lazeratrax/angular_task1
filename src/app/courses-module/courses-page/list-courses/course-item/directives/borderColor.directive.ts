import {
  Directive,
  ElementRef,
  Renderer2,
  Input,
  OnInit,
} from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';


export const DAYS_COLOR: number = -14;
@Directive({
  selector: '[appBorderColor]',
})
export class BorderColorDirective implements OnInit {

  @Input('appBorderColor') date: string;

  private boxShadow: string = '0 1px 24px 0 var(--dark-8)';
  constructor(private el: ElementRef, private r: Renderer2) { }

  public ngOnInit(): void {
    const dateFromDB: number = moment(this.date).toDate().getTime();
    const currentTime: Moment = moment();

    dateFromDB < Date.parse(currentTime.format()) &&
      dateFromDB >= Number(currentTime.add(DAYS_COLOR, 'days'))
      ? (this.boxShadow = '0 1px 24px 0 green')
      : dateFromDB > Date.parse(currentTime.format())
        ? (this.boxShadow = '0 1px 24px 0 blue')
        : (this.boxShadow = '0 1px 24px 0 var(--dark-8)');
    this.r.setStyle(this.el.nativeElement, 'box-shadow', this.boxShadow);
  }
}
