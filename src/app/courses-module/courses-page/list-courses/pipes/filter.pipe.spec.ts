import { FilterPipe } from './filter.pipe';
import * as VARIABLES from './../../../variables';
import { Course } from 'src/app/interfaces/course.interface';

describe('FilterPipe', () => {
  const pipe: FilterPipe = new FilterPipe();
  const testCourse: Course[] = [{
    id: VARIABLES.MOCK_ID,
    isTopRated: true,
    name: VARIABLES.MOCK_TITLE,
    date: VARIABLES.MOCK_CREATION_DATE,
    description: VARIABLES.MOCK_DESCRIPTION,
    length: VARIABLES.MOCK_LENGTH
  }];

  it('filter lowercase', () => {
    expect(pipe.transform(testCourse, 'te')).toEqual(testCourse);
  });

  it('filter uppercase', () => {
    expect(pipe.transform(testCourse, 'TE')).toEqual(testCourse);
  });

  it('filter mixcase', () => {
    expect(pipe.transform(testCourse, 'test')).toEqual(testCourse);
  });
});
