import { Pipe, PipeTransform } from '@angular/core';
import { Course } from '../../../../interfaces/course.interface';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform(courses: Course[], search: string = ''): Course[] {
    if (Boolean(search.trim()) === false) {
      return courses;
    }
    search = search.toLowerCase();
    return courses.filter((course: Course) => {
      return course.description.toLowerCase()
        .includes(search) || course.name.toLowerCase().includes(search);
    });
  }
}
