import { Component, Output, EventEmitter, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { faPlus, IconDefinition } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-search-n-add',
  templateUrl: './search-n-add.component.html',
  styleUrls: ['./search-n-add.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchNAddComponent {
  public faPlus: IconDefinition = faPlus;
  public search: string = '';

  @Output() changeSearch: EventEmitter<string> = new EventEmitter<string>();

  constructor(private router: Router) { }

  public addCourse(): void {
    this.router.navigate(['/courses/new']);
  }

  public onSearchChange(): void {
    this.changeSearch.emit(this.search);
  }
}
