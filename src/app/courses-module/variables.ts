// mocks for tests
export const MOCK_ID: number = 3;
export const MOCK_TITLE: string = `test`;
export const MOCK_CREATION_DATE: string = `2020-09-28T04:39:24+00:00`;
export const MOCK_DESCRIPTION: string = `text description`;
export const MOCK_LENGTH: number = 10;

export const MOCK_DATE: string = '2020-09-28T04:39:24+00:00';
