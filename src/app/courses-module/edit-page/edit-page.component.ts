import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup } from '@angular/forms';
import { Course } from '../../interfaces/course.interface';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
})
export class EditPageComponent implements OnInit {
  form: FormGroup;
  public idCourseToEdit: number;
  public courseToEdit: Course;

  constructor(
    private activateRoute: ActivatedRoute
    ) {
    this.idCourseToEdit = Number(activateRoute.snapshot.url.toString());
  }

  ngOnInit(): void {
  }

  editCourse(): void {
  }
}
