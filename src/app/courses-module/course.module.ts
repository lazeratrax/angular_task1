import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { MainLayoutComponent } from './../main-layout/main-layout.component';

import { CoursesPageComponent } from './courses-page/courses-page.component';
import { SearchNAddComponent } from './courses-page/search-n-add/search-n-add.component';
import { ModalDeleteComponent } from './courses-page/modal-delete/modal-delete.component';

import { ListCoursesComponent } from './courses-page/list-courses/list-courses.component';

import { CourseItemComponent } from './courses-page/list-courses/course-item/course-item.component';
import { BorderColorDirective } from './courses-page/list-courses/course-item/directives/borderColor.directive';
import { DurationPipe } from './courses-page/list-courses/course-item/pipes/duration.pipe';
import { FilterPipe } from './courses-page/list-courses/pipes/filter.pipe';

import { AddPageComponent } from './add-page/add-page.component';
import { AuthorsComponent } from './add-page/authors/authors.component';
import { InputInMinutesComponent } from './add-page/input-in-minutes/input-in-minutes.component';
import { TextInputForNowComponent } from './add-page/text-input-for-now/text-input-for-now.component';

import { EditPageComponent } from './edit-page/edit-page.component';

import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    children: [
      { path: '', component: CoursesPageComponent, canActivate: [AuthGuard] },
      { path: 'new', component: AddPageComponent, canActivate: [AuthGuard] },
      { path: ':id', component: EditPageComponent, canActivate: [AuthGuard] },
    ]
  }
];

@NgModule({
  declarations: [
    CoursesPageComponent,
    SearchNAddComponent,
    ModalDeleteComponent,
    ListCoursesComponent,
    CourseItemComponent,

    AddPageComponent,
    TextInputForNowComponent,
    InputInMinutesComponent,
    AuthorsComponent,

    EditPageComponent,

    BorderColorDirective,
    DurationPipe,
    FilterPipe
  ],
  imports: [RouterModule.forChild(routes),
    FontAwesomeModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [RouterModule],
})
export class CourseModule { }
