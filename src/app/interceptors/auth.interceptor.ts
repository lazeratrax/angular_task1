import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpHeaders,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AuthService } from '../services/authorization.service';
import { User } from '../interfaces/auth.interfaces';

const TOKEN_FROM_LS: string = localStorage.getItem('token');
const DEFAULT_BACKEND_URL: string = `http://localhost:3004`;

@Injectable()
export class AuthInterseptor implements HttpInterceptor {
  constructor(
    private auth: AuthService
  ) { }

  intercept(
    req: HttpRequest<Observable<HttpEvent<Observable<User>>>>,
    next: HttpHandler
  ): Observable<HttpEvent<Observable<User>>> {
    return (
      next
        .handle(req.clone(
          {
            url: `${DEFAULT_BACKEND_URL}${req.url}`,
            headers: new HttpHeaders(this.auth.isAuthenticated() ? {
              Authorization: `${TOKEN_FROM_LS}`
            } : {})
          }
        )
        )
        .pipe(
          tap(() => {
            console.log('intecept');
          })
        )
    );
  }
}

