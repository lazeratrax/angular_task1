import { Injectable } from '@angular/core';
import {
  HttpResponse,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, Subscriber, Subscription } from 'rxjs';
import { LoaderService } from '../services/loader.service';
import { User } from '../interfaces/auth.interfaces';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(private loaderService: LoaderService) { }

  public removeRequest(): void {
    this.loaderService.endLoading();
  }

  public intercept(request: HttpRequest<Observable<HttpEvent<Observable<User>>>>,
                   next: HttpHandler): Observable<HttpEvent<Observable<User>>> {

    this.loaderService.getStartLoading();

    return new Observable((observer: Subscriber<HttpEvent<Observable<User>>>) => {
      const subscription: Subscription = next.handle(request)
        .subscribe(
          (event: HttpEvent<Observable<User>> ) => {
            (event instanceof HttpResponse) ? (this.removeRequest(),
              observer.next(event)) : (console.log('loading... || event not instance of Http Response')
              );
          },
          (err: Error) => { this.removeRequest(); observer.error(err); },
          () => { this.removeRequest(); observer.complete(); });
      return () => {
        this.removeRequest();
        subscription.unsubscribe();
      };
    });
  }
}
