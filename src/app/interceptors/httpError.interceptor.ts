import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { User } from '../interfaces/auth.interfaces';

const STATUS_CLIENT_ERROR_START: number = 400;
const STATUS_CLIENT_ERROR_END: number = 499;
const STATUS_SERVER_ERROR_START: number = 500;
const STATUS_SERVER_ERROR_END: number = 599;
const STATUS_REDERECTION_START: number = 300;
const STATUS_REDERECTION_END: number = 399;
const STATUS_WARNING: number = 401;

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    private toastr: ToastrService
  ) { }

  public showError(error: HttpErrorResponse): void {
    let titleError: string;
    (
      error.status >= STATUS_CLIENT_ERROR_START && error.status <= STATUS_CLIENT_ERROR_END) ? (titleError = `Clientside Error`) :
      (error.status >= STATUS_SERVER_ERROR_START && error.status <= STATUS_SERVER_ERROR_END) ? (titleError = `Serverside Error`) :
        (error.status >= STATUS_REDERECTION_START && error.status <= STATUS_REDERECTION_END) ? (titleError = `Redirection`) :
          (titleError = `Error`);
    this.toastr.error(`${error.status} - ${error.statusText}`, `${titleError}`);
  }

  public showWarning(error: HttpErrorResponse): void {
    this.toastr.warning(`${error.status} - ${error.error}`, `Warring`);
  }

  intercept(
    request: HttpRequest<Observable<HttpEvent<Observable<User>>>>,
    next: HttpHandler): Observable<HttpEvent<Observable<User>>> {

    return next.handle(
      request
    )
      .pipe(
        catchError((error: HttpErrorResponse) => {
          let errorMsg: string = ' ';
          if (error.error instanceof ErrorEvent) {
            console.log(`this error instance of ErrorEvent`);
            errorMsg = `Error: ${error.error.message}`;
          }
          else {
            switch (error.status) {
              case STATUS_WARNING:
                this.showWarning(error);
                break;
              default:
                this.showError(error);
                break;
            }
            errorMsg = `Error Code: ${error.status},  Message: ${error.message}`;
          }
          console.log(errorMsg);
          return throwError(errorMsg);
        }),
      );
  }
}
