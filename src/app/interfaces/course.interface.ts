export interface Course {
  id?: number;
  name: string;
  description: string;
  isTopRated: boolean;
  date: string;
  authors?: Author[];
  length: number;
}

export interface CoursesPageParams {
  start?: number;
  count?: number;
  sort?: string;
  textFragment?: string;
}

export interface Author {
  id: number;
  name: string;
  lastName: string;
}

export interface Authors {
  authors: Author;
}

