
export interface FbAuthResponce {
  idToken: string;
  expiresIn: string;
}

export interface FbCreateResponse {
  name: string;
}

// ---------------------------
export interface Author {
  id: number;
  name: string;
}

export interface Login {
  login: string;
  password: string;
}

export interface Token {
  token: string;
}

export interface Name {
  firstName: string;
  lastName: string;
}

export interface User {
  login: string;
  password: string;
  token?: string;
  id?: number;
  name?: Name;
  returnSecureToken?: boolean;
}
