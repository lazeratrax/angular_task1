import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { LoaderService } from '../services/loader.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainLayoutComponent implements OnInit, OnDestroy {

  public loader$: Observable<boolean>;

  constructor(
    private loaderService: LoaderService
  ) { }

  public ngOnInit(): void {
    this.loader$ = this.loaderService.getLoaderStatus();
  }

  public ngOnDestroy(): void {
    this.loaderService.resetLoaderParams();
  }
}
