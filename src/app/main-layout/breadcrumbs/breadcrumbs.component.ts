import { Component, OnInit } from '@angular/core';
import { Course } from '../../interfaces/course.interface';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.scss'],
})
export class BreadcrumbsComponent implements OnInit {
  public idCourseToShowInBreadcrumbs: number;
  public CourseToShowInBreadcrumbs: Course;

  constructor() {}

  ngOnInit(): void {
  }
}
