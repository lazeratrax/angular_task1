import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import {
  faPlayCircle,
  faUserCircle,
  faSignOutAlt,
  IconDefinition
} from '@fortawesome/free-solid-svg-icons';
import { AuthService } from '../../services/authorization.service';
import { User } from 'src/app/interfaces/auth.interfaces';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public faPlayCircle: IconDefinition = faPlayCircle;
  public faUserCircle: IconDefinition = faUserCircle;
  public faSignOutAlt: IconDefinition = faSignOutAlt;

  public user: User;
  public tokenValueInLocalStorage: string | undefined = localStorage.getItem('token');

  private unsubscribeSource: Subject<void> = new Subject<void>();

  constructor(private authService: AuthService) { }

  public ngOnInit(): void {
    if (Boolean(this.authService.isAuthenticated()) === false) {
      this.authService.getUserInfo({ token: this.tokenValueInLocalStorage })
        .pipe(takeUntil(this.unsubscribeSource))
        .subscribe(
          (user: User) => {
            this.user = user;
          });
    }
  }

  public isAuth(): boolean {
    return !this.authService.isAuthenticated();
  }

  public logOff(): void {
    this.authService.logout();
  }

  public ngOnDestroy(): void {
    this.unsubscribeSource.next();
    this.unsubscribeSource.complete();
  }
}
