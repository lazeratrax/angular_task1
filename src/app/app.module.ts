import { NgModule, Provider } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';

import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterseptor } from './interceptors/auth.interceptor';
import { HttpErrorInterceptor } from './interceptors/httpError.interceptor';
import {LoaderInterceptor} from './interceptors/loader.interceptor';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { LoginPageComponent } from './login-page/login-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';

import { HeaderComponent } from './main-layout/header/header.component';
import { FakeLogoComponent } from './main-layout/header/fake-logo/fake-logo.component';
import { FooterComponent } from './main-layout/footer/footer.component';
import { BreadcrumbsComponent } from './main-layout/breadcrumbs/breadcrumbs.component';

import { MainLayoutComponent } from './main-layout/main-layout.component';
import { MyLoaderComponent } from './my-loader/my-loader.component';

const INTERSEPTOR_PROVIDER_AUTH: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: AuthInterseptor,
};

const INTERSEPTOR_PROVIDER_ERRORS: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: HttpErrorInterceptor,
};

const INTERSEPTOR_LOADER: Provider = {
  provide: HTTP_INTERCEPTORS,
  multi: true,
  useClass: LoaderInterceptor,
};

@NgModule({
  declarations: [
    AppComponent,

    LoginPageComponent,
    NotFoundPageComponent,

    MainLayoutComponent,

    HeaderComponent,
    FakeLogoComponent,
    FooterComponent,
    BreadcrumbsComponent,
    MyLoaderComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule, ReactiveFormsModule, FontAwesomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot()
  ],
  exports: [FontAwesomeModule],
  providers: [INTERSEPTOR_PROVIDER_AUTH,
    INTERSEPTOR_PROVIDER_ERRORS, INTERSEPTOR_LOADER
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
