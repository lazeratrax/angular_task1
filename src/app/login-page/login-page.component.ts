import { Component, OnInit, DoCheck, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from './../services/authorization.service';
import { User } from '../interfaces/auth.interfaces';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoginPageComponent implements OnInit, DoCheck {

  public form: FormGroup;
  public user: User;
  public tokenValueInLocalStorage: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) { }

  public ngOnInit(): void {
    this.form = new FormGroup({
      login: new FormControl(null, [
        Validators.required]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(2)]),
    });
  }

  public ngDoCheck(): void {
    this.tokenValueInLocalStorage = localStorage.getItem('token');
    if (Boolean(this.tokenValueInLocalStorage) === true) {
      this.authService.getUserInfo({ token: this.tokenValueInLocalStorage }).subscribe(
        (formValuesWithToken: User) => {
          this.form.setValue({
            login: formValuesWithToken.login,
            password: formValuesWithToken.password
          });
        });
    }
  }

  public submit(): void {
    if (this.form.invalid) {
      return;
    }
    this.user = {
      login: this.form.value.login,
      password: this.form.value.password
    };
    this.authService.login(this.user).subscribe(
      (user: User) => {
        if (Boolean(user) === true) {
          localStorage.setItem('token', user.token);
          this.router.navigate(['/courses']);
          this.showHelloMessage();
        } else {
          console.log('error token');
        }
      }
    );
  }

  public showHelloMessage(): void {
    this.toastr.success('Hello, user!');
  }
}
